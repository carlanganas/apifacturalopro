<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token')->unique()->nullable();
            $table->enum('role', ['admin', 'user']);
            $table->rememberToken();
            $table->timestamps();
        });

        $user = [
            'name' => 'admin',
            'email' => 'admin@platform.com',
            'email_verified_at' => now(),
            'role' => 'admin',
            'password' => Hash::make(12345678),
        ];
        DB::table('users')->updateOrInsert($user);
        $user = [
            'name' => 'user',
            'email' => 'user@platform.com',
            'email_verified_at' => now(),
            'role' => 'user',
            'password' => Hash::make(12345678),
        ];
        DB::table('users')->updateOrInsert($user);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
