<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Illuminate\Console\Command;

class HostsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'host:crear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un sitio basado en database tenant1.. fqdn debe cambiarse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $website = new Website;
        $md = md5(Carbon::now()->format('Y-m-d H:i:s'));
        $website->uuid = $md;
        $website->managed_by_database_connection = 'tenant1';
        app(WebsiteRepository::class)->create($website);
        $hostname = new Hostname;
        $hostname->fqdn = 'facturalo2.carlangas.online';
        $hostname = app(HostnameRepository::class)->create($hostname);
        app(HostnameRepository::class)->attach($hostname, $website);

        $hostname  = app(\Hyn\Tenancy\Environment::class)->hostname();
        $website   = app(\Hyn\Tenancy\Environment::class)->website();
        echo $hostname." ".$website;
    }
}
