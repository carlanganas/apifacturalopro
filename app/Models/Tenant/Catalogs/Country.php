<?php

namespace App\Models\Tenant\Catalogs;

class Country extends ModelCatalog
{
    public $incrementing = false;
    public $timestamps = false;
}
