<?php

namespace App\Models\Tenant\Catalogs;

class TransportModeType extends ModelCatalog
{
    protected $table = "cat_transport_mode_types";
    public $incrementing = false;
}
