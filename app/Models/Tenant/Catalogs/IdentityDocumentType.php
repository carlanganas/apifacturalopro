<?php

namespace App\Models\Tenant\Catalogs;

class IdentityDocumentType extends ModelCatalog
{
    protected $table = "cat_identity_document_types";
    public $incrementing = false;
}
