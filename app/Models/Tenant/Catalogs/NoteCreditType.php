<?php

namespace App\Models\Tenant\Catalogs;

class NoteCreditType extends ModelCatalog
{
    protected $table = "cat_note_credit_types";
    public $incrementing = false;
}
