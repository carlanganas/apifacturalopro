<?php

namespace App\Models\Tenant\Catalogs;

class PriceType extends ModelCatalog
{
    protected $table = "cat_price_types";
    public $incrementing = false;
}
